import Foundation
import Raylib

class WadHeader{
    var identification: String
    var numOfLumps: Int32
    var dirOffset: Int32

    init(wadData: Data){

        let subdata_ident = wadData.subdata(in: 0..<4)
        let ident: String? = String(data: subdata_ident, encoding: .utf8)

        let subdata_lumps = wadData.subdata(in: 4..<8)
        let lumps = subdata_lumps.withUnsafeBytes { $0.load(as: Int32.self) }
                
        let subdata_offset = wadData.subdata(in: 8..<12)
        let offset = subdata_offset.withUnsafeBytes { $0.load(as: Int32.self) }

        self.identification = ident!
        self.numOfLumps = lumps
        self.dirOffset = offset
    }

    func description() -> String {
        return "==HEADER==\nIdentification: \(identification)\nNumOfLumps: \(numOfLumps)\nDirOffset: \(dirOffset)"
    }
}

class WadDirectory {
    var filepos: Int32
    var size: Int32
    var name: String
    init(wadData: Data, baseOffset: Int32, fileposRange: Range<Int>, sizeRange: Range<Int>, nameRange: Range<Int>){

        let subdataFilepos = wadData.subdata(in: fileposRange)
        let directoryFilepos = subdataFilepos.withUnsafeBytes { $0.load(as: Int32.self) }

        let subdataSize = wadData.subdata(in: sizeRange)
        let directorySize = subdataSize.withUnsafeBytes { $0.load(as: Int32.self) }

        let subdataName = wadData.subdata(in: nameRange)
        let rawStringName: String = String(data: subdataName, encoding: .utf8) ?? "UNKNOWN"
        let directoryName = rawStringName.replacingOccurrences(of: "\0", with: "")

        self.filepos = directoryFilepos
        self.size = directorySize
        self.name = directoryName
    }

    func description() -> String {
        return "==WadDirectory==\nFilepos: \(filepos)\nSize: \(size)\nName: \(name)"
    
    }
}   

class Map {
    var name: String
    var things: WadDirectory
    var linedefs: WadDirectory
    var sidedefs: WadDirectory
    var vertexes: [Vector2]
    var segs: WadDirectory 
    var ssectors: WadDirectory
    var nodes: WadDirectory 
    var sectors: WadDirectory
    var reject: WadDirectory
    var blockmap: WadDirectory
    init(things: WadDirectory, linedefs: WadDirectory, sidedefs: WadDirectory, vertexes: [Vector2], segs: WadDirectory
    , ssectors: WadDirectory, nodes: WadDirectory, sectors: WadDirectory, reject: WadDirectory, blockmap: WadDirectory){
        self.name = things.name
        self.things = things
        self.linedefs = linedefs
        self.sidedefs = sidedefs
        self.vertexes = vertexes
        self.segs = segs
        self.ssectors = ssectors
        self.nodes = nodes
        self.sectors = sectors
        self.reject = reject
        self.blockmap = blockmap
    }
}

/// Wad Header which holds the 12 byte header of the WAD file
class Wad {
    var header: WadHeader
    var directories: [WadDirectory]
    var maps: [Map] = []

    init(header: WadHeader, directories: [WadDirectory], maps: [Map]) {
        self.header = header
        self.directories = directories
        self.maps = maps 
    }

//    func findLump(lump: String) -> Int? {
//        for (index, directory) in directories.enumerated() {
//            if directory.name == lump {
//                return index
//            }
//        }
//        return nil
//    }
//
//    func findMapData(dir: [WadDirectory]) -> [Map]{
//        var map_names: [String] = []
//        var things: [WadDirectory] = []
//        var linedefs: [WadDirectory] = []
//        var sidedefs: [WadDirectory] = []
//        var vertexes: [WadDirectory] = []
//        var segs: [WadDirectory]  = []
//        var ssectors: [WadDirectory] = []
//        var nodes: [WadDirectory] = []    
//        var sectors: [WadDirectory] = []
//        var reject: [WadDirectory] = []
//        var blockmap: [WadDirectory] = []
//        for directory in dir {
//            let regex =  #/^E\dM\d+/#
//            if (try? regex.firstMatch(in: directory.name)) != nil {
//                map_names.append(directory.name)
//            }
//            else
//            {
//                switch directory.name {
//                    case "THINGS":
//                        things.append(directory)
//                    case "LINEDEFS":
//                        linedefs.append(directory)
//                    case "SIDEDEFS":
//                        sidedefs.append(directory)
//                    case "VERTEXES":
//                        vertexes.append(directory)
//                    case "SEGS":
//                        segs.append(directory)
//                    case "SSECTORS":
//                        ssectors.append(directory)
//                    case "NODES":
//                        nodes.append(directory)
//                    case "SECTORS":
//                        sectors.append(directory)
//                    case "REJECT":
//                        reject.append(directory)
//                    case "BLOCKMAP":
//                        blockmap.append(directory)
//                    default:
//                        continue
//                }
//            }
//        }
//
//        for (index, _) in map_names.enumerated(){
//            let map = Map(things: things[index], linedefs: linedefs[index], sidedefs: sidedefs[index], vertexes: vertexes[index]
//            , segs: segs[index], ssectors: ssectors[index], nodes: nodes[index], sectors: sectors[index]
//            , reject: reject[index], blockmap: blockmap[index])
//            maps.append(map)
//        }
//        return maps
//    }
//
    func description() -> String {
        var desc_string = header.description()
        for dir in directories {
            desc_string += "\n" + dir.description()
        }
        return desc_string
    }
}


func extractVertexData(pos: Int32, size: Int32) -> [Vector2]
{
    var vectorList = [Vector2]()

    let vertRange = Int(pos)..<Int(pos + size)
    let vertexData = wadData.subdata(in:vertRange) 
    let vertexCount = size / 4
    for i in 0..<vertexCount {
        let xRange = Int(i*4)..<Int(i*4 + 2)
        let yRange = Int(i*4 + 2)..<Int(i*4 + 4)
        let x = vertexData.subdata(in: xRange).withUnsafeBytes { $0.load(as: Int16.self) }
        let y = vertexData.subdata(in: yRange).withUnsafeBytes { $0.load(as: Int16.self) }
        vectorList.append(Vector2(x: Float(x), y: Float(y)))
    }
    return vectorList
}

let fileURL = URL(fileURLWithPath: "doom.wad")

let wadData = try Data(contentsOf: fileURL)
let header = WadHeader(wadData: wadData)
let offset = header.dirOffset
let lumps = header.numOfLumps
var dirs: [WadDirectory] = []


for i in 0..<Int(lumps) {
    let baseOffset = Int(offset) + 16 * i
    let fileposRange = baseOffset..<(baseOffset + 4)
    let sizeRange = (baseOffset + 4)..<(baseOffset + 8)
    let nameRange = (baseOffset + 8)..<(baseOffset + 16)
    let dir = WadDirectory(wadData: wadData, baseOffset: Int32(baseOffset)
    , fileposRange: fileposRange, sizeRange: sizeRange, nameRange: nameRange)
    dirs.append(dir)
}


var map_names: [String] = []
var things: [WadDirectory] = []
var linedefs: [WadDirectory] = []
var sidedefs: [WadDirectory] = []
var vertexes: [[Vector2]] = []
var segs: [WadDirectory]  = []
var ssectors: [WadDirectory] = []
var nodes: [WadDirectory] = []    
var sectors: [WadDirectory] = []
var reject: [WadDirectory] = []
var blockmap: [WadDirectory] = []
for dir in dirs {
    let regex =  #/^E\dM\d+/#
    if (try? regex.firstMatch(in: dir.name)) != nil {
        map_names.append(dir.name)
        }
        else
        {
            switch dir.name {
                case "THINGS":
                    things.append(dir)
                case "LINEDEFS":
                    linedefs.append(dir)
                case "SIDEDEFS":
                    sidedefs.append(dir)
                case "VERTEXES":
                let vertexData = extractVertexData(pos: dir.filepos, size: dir.size)
                vertexes.append(vertexData)
                case "SEGS":
                    segs.append(dir)
                case "SSECTORS":
                    ssectors.append(dir)
                case "NODES":
                    nodes.append(dir)
                case "SECTORS":
                    sectors.append(dir)
                case "REJECT":
                    reject.append(dir)
                case "BLOCKMAP":
                    blockmap.append(dir)
                default:
                    continue
        }
    }
}
var mapList = [Map]()
//enumerate map name
for (index, _) in map_names.enumerated(){
    let map = Map(things: things[index], linedefs: linedefs[index], sidedefs: sidedefs[index], vertexes: vertexes[index]
    , segs: segs[index], ssectors: ssectors[index], nodes: nodes[index], sectors: sectors[index]
    , reject: reject[index], blockmap: blockmap[index])
    mapList.append(map)
}

// Find min and max coordinates for scaling
var minX: Float = Float.infinity
var maxX: Float = -Float.infinity
var minY: Float = Float.infinity
var maxY: Float = -Float.infinity

let vertices = mapList[0].vertexes // Assuming mapList[0] is your first map

for vertex in vertices {
    minX = min(minX, vertex.x)
    maxX = max(maxX, vertex.x)
    minY = min(minY, vertex.y)
    maxY = max(maxY, vertex.y)
}

let screenWidth: Int32 = 1280
let screenHeight: Int32 = 720
// Calculate scale factor and translation to fit the map in the window with some margin
let margin: Float = 50.0 // Margin around the map in pixels
let scaleX = (Float(screenWidth) - 2 * margin) / (maxX - minX)
let scaleY = (Float(screenHeight) - 2 * margin) / (maxY - minY)
let scale = min(scaleX, scaleY) // To maintain aspect ratio
let translateX = (Float(screenWidth) - (maxX - minX) * scale) / 2.0 - minX * scale
let translateY = (Float(screenHeight) - (maxY - minY) * scale) / 2.0 - minY * scale


Raylib.initWindow(screenWidth, screenHeight, "Doom Map")
Raylib.setTargetFPS(30)
while Raylib.windowShouldClose == false {
    Raylib.beginDrawing()
    Raylib.clearBackground(.rayWhite)
    
    // Draw each vertex as a small rectangle or point for now
    for vertex in vertices {
        let x = vertex.x * scale + translateX
        let y = vertex.y * scale + translateY
        Raylib.drawCircle(Int32(x), Int32(y), 3, .black) // You can adjust the size and color
    }
    
    // TODO: Draw lines between vertices according to the map's linedefs

    Raylib.endDrawing()
}
Raylib.closeWindow()